const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function (req, res, next){
    console.log(req);
    const token = req.header('x-auth-token') || req.header['x-auth-token']; // get token form request header 
    if(!token) return res.status(405).json(req)

    try {
        const decoded = jwt.verify(token, config.get('jwtSecret'));
        req.user = decoded.user; // if there is a token valid,  i will include it as part of the request 
        next(); // moving to next pieace of code = do the next stuff of behaviour
    } catch (error) {
        console.log('middleware error ----> ' ,  error)
        res.status(401).json({msg: 'Token is not Valid'});
    }
}