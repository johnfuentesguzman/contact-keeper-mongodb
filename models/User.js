const mongoose =  require('mongoose');

const UserSchema= mongoose.Schema({
    name:{
        type: String,
        require: true,
    },
    email:{
        type: String,
        require: true,
        unique: true
    },
    password:{
        type: String,
        require: true,
    },
    date:{
        type: Date,
        default: Date.now() // if this data is empty.. now will be taken by default
    },
});

module.exports = mongoose.model('user', UserSchema); // parameters =  collection name, schema