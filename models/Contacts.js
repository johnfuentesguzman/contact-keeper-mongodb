const mongoose =  require('mongoose');

const ContactsSchema= mongoose.Schema({
    user:{
        type: mongoose.Schema.Types.ObjectId, // in the mongo collection you will see this shape/state
        ref:'users' // bring all from a specific collection .. this case users
    },
    name:{
        type: String,
        require: true,
    },
    email:{
        type: String,
        require: true,
    },
    phone:{
        type: String,
        require: true,
    },
    type:{
        type: String,
        default: 'personal'
    },
    date:{
        type: Date,
        default: Date.now() // if this data is empty.. now will be taken by default
    },
});

module.exports = mongoose.model('contact', ContactsSchema); // parameters =  collection name, schema