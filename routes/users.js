const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const { check, validationResult } = require('express-validator');
const User = require('../models/User');

// @route  POST api/users
// @desc register users
// @access Public
router.post('/',
    [    //validations of body payload
        check('name', 'Name is Required').not().isEmpty(),
        check('email', 'Please include a Valid email').isEmail(),
        check('password', 'Please enter a passoword with atleast 6 characters').isLength({ min: 6 })
    ],
    async (req, resp) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return resp.status(402).json({ errors: errors.array() }); // it returns an array with a detailed payload
        }
        const { name, email, password } = req.body;
        try {
            //***usign await , because these functions return promise, and like that we avoid use then/catch statement */

            let user = await User.findOne({ email }) // // finding in the model or database collection "User".. this case "email"
            if (user) {
                return resp.status(400).json({ msg: 'User already Exists' });
            }
            user = new User({ name, email, password }); // model

            const salt = await bcrypt.genSalt(10);
            user.password = await bcrypt.hash(password, salt); //encrypting/hashi password
            await user.save(); // saving user on DB

            const payload = {
                user: {
                    id: user.id
                }
            }

            jwt.sign(payload, config.get('jwtSecret'),
            {
                expiresIn: 3600 // time when the token expires
            },
            (err, token) => {
                if (err) throw err;
                resp.json({ token });
            }
            );

            // resp.send('Congrats, your user has been created');
        } catch (error) {
            console.error(error);
            resp.status(500).json(error)
        }

    });

module.exports = router;