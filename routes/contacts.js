const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const User = require('../models/User');
const Contact = require('../models/Contacts');
const auth = require('../middleware/auth');

// @route  POST api/contacts
// @desc  ADD a new contact
// @access Private (you need to be logged)
// params: route --> /, auth --> middelware, validations of payload
router.post('/', [auth,
    [
        check('name', 'Name is Required').not().isEmpty(),
    ]],
    async (req, resp) => {
        const errors = validationResult(req); // validating payload data --> body
        if (!errors.isEmpty()) return resp.status(400).json({ error: errors.array() });
        const { name, email, phone, type } = req.body;
        try {
            const newContact = new Contact({ name, email, phone, type, user: req.user.id });// saving the data.. user comes from "auth" --> middleware
            const contact = await newContact.save();
            resp.json(contact); //return contact saved
        } catch (error) {
            console.error('save new contact - contact route ---> ', error);
            resp.status(500).json(error)
        }
    });

// @route  GET api/contacts
// @desc GET all user contacts
// @access Private (you need to be logged)
router.get('/', auth,
    async (req, resp) => {
        try {
            const contacts = await Contact.find({ user: req.user.id }).sort({ date: -1 }); // Bring the latest contacts first
            resp.json(contacts);

        } catch (error) {
            console.error('get all contacts - contact route ---> ', error);
            resp.status(500).json(error)
        }
    });

// @route  GET api/contacts/:id 
// @desc  PUT  Update contact
// @access Private (you need to be logged)
router.put('/:id', auth,
    async (req, resp) => {
        const { name, email, phone, type } = req.body;
        const contactFields = {};
        if (name) contactFields.name = name;
        if (email) contactFields.email = email;
        if (phone) contactFields.phone = phone;
        if (type) contactFields.type = type;

        try {
            console.warn(req.param.id);
            let contact = await Contact.findById(req.params.id);
            if (!contact) return resp.status(404).json({ msg: 'Contact not Found' });
            if (contact.user.toString() !== req.user.id) return resp.status(401).json({ msg: 'Not Authorized' });

            contact = await Contact.findByIdAndUpdate(
                req.params.id,// finding if the id exist
                { $set: contactFields },  // if this contact exist then UPDATE it
                { new: true }); // if this contact NO exist then CREATE it

            resp.json(contact); // returning contact created
        } catch (error) {
            console.error('updating contact - contact route ---> ', error);
            resp.status(500).json(error)
        }
    });


// @route  DELETE api/contacts/:id 
// @desc   Delete contact
// @access Private (you need to be logged)
router.delete('/:id', auth,
    async (req, resp) => {
        try {
            let contact = await Contact.findById(req.params.id);
            if (!contact) return resp.status(404).json({ msg: 'Contact not Found' });
            if (contact.user.toString() !== req.user.id) return resp.status(401).json({ msg: 'Not Authorized' });

            await Contact.findByIdAndRemove(req.params.id);
            resp.json('Contact Removed'); // returning contact created
        } catch (error) {
            console.error('updating contact - contact route ---> ', error);
            resp.status(500).json(error)
        }
    });


module.exports = router;