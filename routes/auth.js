const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const auth = require('../middleware/auth');
const { check, validationResult } = require('express-validator');
const User = require('../models/User');

// @route  GET api/auth
// @desc GET logged user
// @access Private
// auth param  = middelware --> validate if there is token
router.get('/', auth,
  async (req, resp) => {
    try {
        const user = await User.findById(req.user.id).select('-password') // select('-password') = i dont want to return the passport
        resp.json(user);
    } catch (error) {
        console.log('auth routes GET ----> ', error);
        resp.status(500).send().json({error: 'server error'});
    }
});

// @route  POST api/auth
// @desc  AUth user and Get token
// @access Public 
router.post('/', [
    //validations of body payload
    check('email', 'Please include a Valid Email').isEmail(),
    check('password', 'Password is Required').not().isEmpty(),
],
async (req, resp) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return resp.status(402).json({ errors: errors.array() }); // it returns an array with a detailed payload
    }
    const {email, password} = req.body;

    try {
        let user = await User.findOne({email}); // finding in the model or database collection "User"
        if (!user) {
            return resp.status(400).json({ msg: 'Invalid Credentials' });
        }
        const isMath = await bcrypt.compare(password, user.password);
        if(!isMath){
            return resp.status(400).json({msg: 'Invalid Credentials'});
        };

        
        const payload = {
            user: {
                id: user.id
            }
        }

        jwt.sign(payload, config.get('jwtSecret'),
        {
            expiresIn: 3600 // time when the token expires
        },
        (err, token) => {
            if (err) throw err;
            resp.json({ token });
        }
        );

        
    } catch (error) {
        console.log('auth error --> ', error);
        resp.status(500).send().json({error: 'Server error'});
    }
});

module.exports = router;