import React, { useReducer } from 'react';
import axios from 'axios';
import uuid from 'uuid';
import setAuthToken from '../../utils/setAuthToken';

import ContactContext from './contactContext';
import contactReducer from './contactReducer';
import {
    GET_CONTACTS,
    ADD_CONTACT,
    DELETE_CONTACT,
    SET_CURRENT,
    CLEAR_CURRENT,
    UPDATE_CONTACT,
    FILTER_CONTACTS,
    CLEAR_CONTACTS,
    CLEAR_FILTER,
    CONTACT_ERROR,
    SET_ALERT,
    REMOVE_ALERT
} from '../types';

const ContactState = props => {
    const initialState = {
        contacts: null,
        current: null,
        filtered: null,
        error: null
    };
    const [state, dispatch] = useReducer(contactReducer, initialState);

    //Get all Contacts
    const getContacts = async (contact) => {
        if (localStorage.token) {
            var config = setAuthToken(localStorage.token);
        }
        try {
            const res = await axios.get('api/contacts', config);
            dispatch({ type: GET_CONTACTS, payload: res.data });

        } catch (error) {
            dispatch({ type: CONTACT_ERROR, payload: error })
        }
    };


    //Add Contact
    const addContact = async (contact) => {
        // contact.id = uuid.v4();// adding unique id to contact created
        if (localStorage.token) {
            var config = setAuthToken(localStorage.token);
        }
        try {
            const res = await axios.post('api/contacts', contact, config);
            dispatch({ type: ADD_CONTACT, payload: res.data });

        } catch (error) {
            dispatch({ type: CONTACT_ERROR, payload: error })
        }

    };

    //Update Contact
    const updateContact = async (contact) => {
                // contact.id = uuid.v4();// adding unique id to contact created
                if (localStorage.token) {
                    var config = setAuthToken(localStorage.token);
                }
                try {
                    const res = await axios.put(`api/contacts/${contact._id}`, contact, config);
                    dispatch({ type: UPDATE_CONTACT, payload: res.data });
        
                } catch (error) {
                    dispatch({ type: CONTACT_ERROR, payload: error })
                }
    };

    //Delete Contact
    const deleteContact = async (id) => {
        if (localStorage.token) {
            var config = setAuthToken(localStorage.token);
        }
        try {
            const res = await axios.delete(`api/contacts/${id}`, config);
            dispatch({ type: DELETE_CONTACT, payload: id });

        } catch (error) {
            dispatch({ type: CONTACT_ERROR, payload: error })
        }

    };


    //Set current Contact
    const setCurrent = (contact) => {
        dispatch({ type: SET_CURRENT, payload: contact });
    };

    //Clear current Contact
    const clearCurrent = () => {
        dispatch({ type: CLEAR_CURRENT });
    };


    // Filter Contact
    const filterContacts = (text) => {
        dispatch({ type: FILTER_CONTACTS, payload: text });
    };

    // Filter Contact
    const clearContacts = () => {
        dispatch({ type: CLEAR_CONTACTS });
    };

    // Clear Filter
    const clearFilter = () => {
        dispatch({ type: CLEAR_FILTER });
    };

    return (
        <ContactContext.Provider
            value={{
                contacts: state.contacts, // Data payload
                current: state.current, // Data payload
                filtered: state.filtered, // Data payload
                error: state.error, // Data payload
                getContacts, // Funcition
                addContact, // Function
                deleteContact, // Function
                setCurrent, // Function
                clearCurrent, // Function
                updateContact,  // Function
                clearContacts, // Function,
                filterContacts, // Function
                clearFilter // Function
            }}
        >

            {props.children}
        </ContactContext.Provider>
    );
}

export default ContactState;
