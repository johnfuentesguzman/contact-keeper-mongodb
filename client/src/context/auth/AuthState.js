import React, { useReducer } from 'react';
import uuid from 'uuid';
import axios from 'axios';
import AuthContext from './authContext';
import authReducer from './authReducer';
import setAuthToken from '../../utils/setAuthToken';

import {
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    USER_LOADED,
    AUTH_ERROR,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    CLEAR_FILTER,
    LOGOUT,
    CLEAR_ERRORS
} from '../types';

const AuthState = props => {
    const initialState = {
        token: localStorage.getItem('token'),
        isAuthenticated: null,
        user: null,
        loading: true,
        error: null
    };
    const [state, dispatch] = useReducer(authReducer, initialState);

    //Load user
    const loadUser = async () => { // get data form user owner/logged saved on database/mongo
        if (localStorage.token){
            var config = setAuthToken(localStorage.token);
        }

        try {
            const resp = await axios.get('/api/auth', config);
            dispatch({type: USER_LOADED, payload: resp.data});
        } catch (error) {
            dispatch({type: AUTH_ERROR});
        }

    };

    //Register User
    const register = async (formData) => {
        console.warn('register function heyy ', formData);
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        try {
            const resp = await axios.post('api/users', formData, config);
            dispatch({ type: REGISTER_SUCCESS, payload: resp.data }); //res.data will have the token for the user created

            loadUser(); // get saved users to show them in home page
        } catch (error) {
            dispatch({ type: REGISTER_FAIL, payload: error.response.data.msg });
        }
    }

    // Login User
    const login = async (formData) => {
        console.warn('register function heyy ', formData);
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        try {
            const resp = await axios.post('api/auth', formData, config);
            dispatch({ type: LOGIN_SUCCESS, payload: resp.data }); //res.data will have the token for the user created

            loadUser(); // get saved users to show them in home page
        } catch (error) {
            dispatch({ type: LOGIN_FAIL, payload: error.response.data.msg });
        }
    }

    // Logout User
    const logout = () => dispatch({type:LOGOUT});

    //Clear errors
    const clearErrors = () => dispatch({type: CLEAR_ERRORS});

    return (
        <AuthContext.Provider
            value={{
                token: state.token, // Data payload
                isAuthenticated: state.isAuthenticated, // Data payload
                loading: state.loading, // Data payload
                user: state.user, // Data payload
                error: state.error, // Data payload
                register, // function
                loadUser, // function
                login, // function
                logout, // function
                clearErrors // function
            }}
        >
            {props.children}
        </AuthContext.Provider>
    );
};
export default AuthState;
