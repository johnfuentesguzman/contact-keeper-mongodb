import React, {useReducer} from 'react';
import uuid from 'uuid';
import AlertContext from './alertContext';
import alertReducer from './alertReducer';
import {SET_ALERT, REMOVE_ALERT} from '../types';

const AlertState = props => {
    const initialState = [];
    const [state, dispatch] = useReducer(alertReducer, initialState);

    // Set alert
    const setAlert = (mss, type, timeout = 5000) => {
        const id = uuid.v4();
        dispatch({type: SET_ALERT, payload: {mss, type, id}});

        setTimeout(() => {
            dispatch({type: REMOVE_ALERT, payload: id });
        }, timeout);
    };

    return (
        <AlertContext.Provider
            value = {{
                alerts: state, // Data payload, this case an Array
                setAlert // function
            }}
        >   

        {props.children}
        </AlertContext.Provider>  
    );
}

export default AlertState;
