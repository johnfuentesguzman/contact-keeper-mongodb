import React from 'react'

export const About = () => {
    return (
        <div>
            <h1>About Page</h1>
            <p className="my-1"> Full stack react project: Mongo express</p>
            <p className="bg-dark p">
                <strong>Version: </strong> 1.0.0
            </p>
        </div>
    )
}
export default About;