import React, { Fragment, useContext, useEffect } from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import ContactContext from '../../context/contact/contactContext';
import ContactItem from '../../components/contacts/ContactItem';
import Spinner from '../layout/Spinner';


export const Contacts = () => {
    const contactData = useContext(ContactContext);
    const { contacts, filtered, getContacts, loading } = contactData;

    useEffect(() => {
        getContacts();
        // eslint-disabled-next-line∫
    }, [])

    if (contacts!== null && contacts.length === 0 && loading) {
        return <h4>Please Add a a contact</h4>
    }

    return (
        <Fragment>
            {contacts !== null && !loading ? (<TransitionGroup>
                {filtered !== null

                    ? filtered.map(contact => (<CSSTransition key={contact.id} timeout={500}><ContactItem key={contact.id} contactDetail={contact} /></CSSTransition>))

                    : contacts.map(contact => (<CSSTransition key={contact.id} timeout={500} ><ContactItem contactDetail={contact} /></CSSTransition>))
                }
            </TransitionGroup>) : <Spinner />}

        </Fragment>
    );
}

export default Contacts;