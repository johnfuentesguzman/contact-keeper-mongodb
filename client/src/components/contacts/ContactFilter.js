import React, {useContext, useRef, useEffect} from 'react'
import ContactContext from '../../context/contact/contactContext';

export const ContactFilter = () => {
    const contactContext = useContext(ContactContext) // Here we can access to any mettod or state in the app
    const text = useRef('');
    const { filtered, filterContacts, clearFilter} =  contactContext // Destructuring functions/data availables in all app

    useEffect(() => {
        if(filtered ===  null){
            text.current.value = '';      }
    }, [contactContext, filtered])

    const onChange = (e) => {
        if (text.current.value !== ''){
            filterContacts(text.current.value);
        }else{
            clearFilter();
        }
    };
    return (
        <form>
            <input ref={text} type='text' placeholder='Filter Contacts' onChange={onChange} />
        </form>
    )
}
export default ContactFilter;
