import React,  {useContext} from 'react';
import PropTypes from 'prop-types';
import ContactContext from '../../context/contact/contactContext';

export const ContactItem = ({contactDetail}) => {
    const contactContext = useContext(ContactContext) // Here we can access to any mettod or state in the app
    const { deleteContact, setCurrent, clearCurrent } =  contactContext // Destructuring functions/data availables in all app

    const {_id, name, email, phone, type} = contactDetail;

    
    const onDelete = () => {
        deleteContact(_id); // Deleting contact data,"onclick function"
        clearCurrent(); // 
    };

    return (
        <div className='card bg-light'>
           <h3 className='text-primary text-left'>{name} 
            <span className={'badge ' + (type === 'professional' ?  'badge-success' : 'badge-primary')}>
                {type.charAt(0).toUpperCase() + type.slice(1)}
            </span>
            <ul className='list'>
                {email && (
                    <li><i className="fas fa-envelope-open"></i>
                        {email}
                    </li>
                )}
                {phone && (
                    <li className='fas fa-snowboarding'>
                        {phone}
                    </li>
                )}
            </ul>
            <p className='btn btn-dark btn-sm' onClick={() => setCurrent(contactDetail)}>Edit</p>
            <p className='btn btn-danger btn-sm' onClick={onDelete}>Delete</p>
            </h3>
        </div>
    )
}
export default ContactItem;