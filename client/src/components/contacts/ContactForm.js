import React, { useState, useContext, useEffect } from 'react';
import ContactContext from '../../context/contact/contactContext'
import { UPDATE_CONTACT } from '../../context/types';

export const ContactForm = () => {
    const contactContext = useContext(ContactContext) // Here we can access to any mettod or state in the app
    const { addContact, current, clearCurrent, updateContact } = contactContext; // Destructuring functions/data availables in all app

    useEffect(() => { // Hook to make/mimify the componentdidmount lifecycle
        if (current !== null) {
            setContact(current); // set the form with current data at the time when the form is loaded
        } else {
            setContact({
                name: '', email: '', phone: '', type: 'personal'
            }); // Once the contact data has been update , we back the form to default values
        };
    }, [contactContext, current]);

    const [contact, setContact] = useState({ // Paramters: data to set, function which set the data just "live" here             
        name: '',
        email: '',
        phone: '',
        type: 'personal'
    });// This case state has these values as default..

    const { name, email, phone, type } = contact;

    const onChange = (e) => {
        setContact({ ...contact, [e.target.name]: e.target.value }) // Whatever the input by name has been selected
    };

    const clearAll = () => {
        clearCurrent();
    };

    const onSubmit = (e) => {
        e.preventDefault();
        if(current === null){
            addContact(contact) // Adding contact data, previously update in "onChange function"
        }else{
            updateContact(contact); // Updating contact data, previously loaded in "onChange function"
        }
        clearAll();
    };

    return (
        <form onSubmit={onSubmit}>
            <h2 className='text-primary'>{current ? 'Update Contact' : 'Add Contact'}</h2>
            <input type='text' placeholder='Name' name='name' value={name} onChange={onChange} />
            <input type='email' placeholder='Email' name='email' value={email} onChange={onChange} />
            <input type='text' placeholder='Phone' name='phone' value={phone} onChange={onChange} />
            <h5>Contact Type</h5>
            <input type='radio' name='type' value='personal' checked={type === 'personal'} onChange={onChange} /> Personal{' '}
            <input type='radio' name='type' value='professional' checked={type === 'professional'} onChange={onChange} /> Professional{' '}

            <div>
                <input type='submit' value={current ? 'Update Contact' : 'Add Contact'} className='btn btn-primary btn-block' />
            </div>
            {current && (
                <div>
                    <button className='btn btn-light btn-block'  onClick={clearAll}>Clear</button>
                </div>
            )}

        </form>
    )
}

export default ContactForm;
