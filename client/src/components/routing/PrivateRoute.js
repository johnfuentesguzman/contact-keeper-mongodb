import React, { useContext } from 'react';
import { Route, Redirect} from 'react-router-dom';
import AuthContext from '../../context/auth/authContext';

export const PrivateRoute = ({component: Component, ...rest}) => {
    const authContext = useContext(AuthContext);
    const { isAuthenticated, loading } = authContext; // Destructuring functions/data availables in all app
    return (
        <Route {...rest} render={props => !isAuthenticated && !loading ? (
        <Redirect to='/Login' />
        ) : (
           <Component  {...props}/> 
        )}            
        />
    )
}

export default PrivateRoute;