import React, { Fragment, useContext } from 'react'
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import AuthContext from '../../context/auth/authContext';
import ContactContext from '../../context/contact/contactContext';

export const Navbar = ({ title, icon }) => {
    const authContext = useContext(AuthContext);
    const contactContext = useContext(ContactContext);
    const { isAuthenticated, logout, user } = authContext; // Destructuring functions/data availables in all app
    const { clearContacts} = contactContext; // Destructuring functions/data availables in all app

    const onLogout =() => {
        logout();
        clearContacts();
    };

    const authLinks = (
        <Fragment>
            <li>Hello {user && user.name}</li>
            <li>
                <a onClick={onLogout}>
                    <icon className='fas fa-sign-out-alt'></icon><span className='hide-sm'>Logout</span>
                </a>
            </li>
        </Fragment>

    )
    const guestLinks = (
        <Fragment>
            <li>Hello {user && user.name}</li>
            <li>
                <Link to='/register'>Register</Link>
            </li>
            <li>
                <Link to='/login'>Login</Link>
            </li>
        </Fragment>

    )
    return (
        <div className='navbar bg-primary'>
            <ul>
                {isAuthenticated ? authLinks: guestLinks}
            </ul>
        </div>
    )
}
Navbar.propTypes = {
    title: PropTypes.string.isRequired,
    icon: PropTypes.string
}

Navbar.defaultProps = {
    title: 'Contact Keeper',
    icon: 'fas fas-id-card-alt'
}
export default Navbar;