import React, {  useState, useContext, useEffect } from 'react'
import AlertContext from '../../context/alert/alertContext';
import AuthContext from '../../context/auth/authContext';

export const Login = (props) => {
    const alertContext = useContext(AlertContext) // Here we can access to any mettod or state in the app
    const authContext = useContext(AuthContext) // Here we can access to any mettod or state in the app

    const { setAlert } = alertContext // Destructuring functions/data availables in all app
    const { login, error, clearErrors, isAuthenticated } = authContext // Destructuring functions/data availables in all app
    useEffect(() => {
        if(isAuthenticated){
            props.history.push('/') // if the user is already logged , we will show/redirect to home page (props.history)
        }
        if (error === 'Invalid Credentials'){
            setAlert(error, 'danger');
            clearErrors();
        }
        // eslint-disable-next-line
    }, [error, isAuthenticated, props.history]);

    const [user, setUser] = useState({ // Paramters: data to set, function which set the data just "live" here             
        email: '',
        password: '',

    });

    const { password, email} = user;

    const onChange = (e) =>{
        setUser({...user, [e.target.name]: e.target.value}); // Whatever the input by name has been selected
    };

    const onSubmit = (e) =>{
       e.preventDefault();
       if(email === '' || password === ''){
           setAlert('Please fill in  all the fields', 'danger');
       }else{
           login({email,password})
       }
    };

    return (
        // htmlFor is used because  this is "JSX"
        <div className='form-container'>
            <h1>
                <span className='text-primary'>Login Form</span>
                <form  onSubmit={onSubmit}>
                    <div className='form-group'>
                        <label htmlFor='email'>Email</label>
                        <input type='email' name='email' value={email} onChange={onChange} required></input>
                    </div>
                    <div className='form-group'>
                        <label htmlFor='password'>Password</label>
                        <input type='password' name='password' value={password} onChange={onChange} required></input>
                    </div>
                    <input type='submit' value='Login' className='btn btn-primary btn-block'></input>
                </form>
            </h1>

        </div>
    )
}
export default Login;