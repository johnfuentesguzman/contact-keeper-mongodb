import React, { useState, useContext, useEffect } from 'react';
import AlertContext from '../../context/alert/alertContext';
import AuthContext from '../../context/auth/authContext';

export const Register = (props) => {
    const alertContext = useContext(AlertContext) // Here we can access to any mettod or state in the app
    const authContext = useContext(AuthContext) // Here we can access to any mettod or state in the app

    const { setAlert } = alertContext // Destructuring functions/data availables in all app
    const { register, error, clearErrors, isAuthenticated } = authContext // Destructuring functions/data availables in all app

    useEffect(() => {
        if(isAuthenticated){
            props.history.push('/') // if the user is already logged , we will show/redirect to home page (props.history)
        }
        if (error === 'User already Exists'){
            setAlert(error, 'danger');
            clearErrors();
        }
        // eslint-disable-next-line
    }, [error, isAuthenticated, props.history]);


    const [user, setUser] = useState({ // Paramters: data to set, function which set the data just "live" here             
        name: '',
        email: '',
        password: '',
        password2: ''

    });
    const { name, email, password, password2 } = user;

    const onChange = (e) => {
        setUser({ ...user, [e.target.name]: e.target.value }); // Whatever the input by name has been selected
    };

    const onSubmit = (e) => {
        e.preventDefault();
        if (name === '' || email === '' || password === '') {
            setAlert('Please enter all fields', 'danger')
        }
        else if (password !== password2) {
            setAlert('Passwords dont match', 'danger')
        } else {
            register({ name, email, password });
        }
    };

    return (
        // htmlFor is used because  this is "JSX"
        <div className='form-container'>
            <h1>
                <span className='text-primary'>Register Form</span>
                <form onSubmit={onSubmit}>
                    <div className='form-group'>
                        <label htmlFor='name'>Name</label>
                        <input type='text' name='name' value={name} onChange={onChange} required></input>
                    </div>
                    <div className='form-group'>
                        <label htmlFor='email'>Email</label>
                        <input type='email' name='email' value={email} onChange={onChange} required></input>
                    </div>
                    <div className='form-group'>
                        <label htmlFor='password'>Password</label>
                        <input type='password' name='password' value={password} onChange={onChange} required minLength='6'></input>
                    </div>
                    <div className='form-group'>
                        <label htmlFor='password2'>Confirm Password</label>
                        <input type='password' name='password2' value={password2} onChange={onChange} required></input>
                    </div>
                    <input type='submit' value='Register' className='btn btn-primary btn-block' required minLength='6'></input>
                </form>
            </h1>

        </div>
    )
}
export default Register;