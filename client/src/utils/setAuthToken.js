import axios from 'axios';
const { version } = require('axios/package');

console.log(`Axios version: ${version}`);

// why this??? this fuction tracks if there is a token present in the HTTP request
// yes: then we set on header for each request
// no: delete it
const adapter = axios.create({});


const setAuthToken = (token) => {
  return {
    headers: {
        'Content-Type': 'application/json',
        'x-auth-token': token
    }
  }
}

export default setAuthToken;