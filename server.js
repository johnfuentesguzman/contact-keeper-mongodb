const express = require('express');
const connectDB = require('./config/db');

const app = express();
connectDB(); //connect to Mongo Database
app.use(express.json({extended: false})) // init middleware -> it allows to use data in body request

app.get('/', (req, res) => res.json({msg: 'Welcome to contact keeper project API'}));

// Defines Routes and calling specific manage files
app.use('/api/users', require('./routes/users'));
app.use('/api/auth', require('./routes/auth'));
app.use('/api/contacts', require('./routes/contacts'));

const PORT=  process.env.PORT || 5000;
app.listen(PORT, () => console.log('server started on PORT ', PORT));